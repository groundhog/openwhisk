#!/bin/bash
# File              : ansible.sh
# Author            : Mohamed Alzayat <alzayat@mpi-sws.org>
# Date              : 09.10.2020
# Last Modified Date: 11.10.2020
# Last Modified By  : Mohamed Alzayat <alzayat@mpi-sws.org>
#
# Licensed to the Apache Software Foundation (ASF) under one or more
# contributor license agreements.  See the NOTICE file distributed with
# this work for additional information regarding copyright ownership.
# The ASF licenses this file to You under the Apache License, Version 2.0
# (the "License"); you may not use this file except in compliance with
# the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

set -e
set -x

sudo pip3 install --upgrade setuptools pip
sudo apt-get install -y software-properties-common
#sudo apt-add-repository -y ppa:ansible/ansible
sudo apt-get update
sudo apt-get install -y python-dev libffi-dev libssl-dev
sudo pip3 install markupsafe
sudo pip3 install ansible==2.9
sudo pip3 install jinja2==2.9.6
sudo pip3 install docker==2.2.1    --ignore-installed  --force-reinstall
sudo pip3 install httplib2==0.9.2  --ignore-installed  --force-reinstall
sudo pip3 install requests==2.10.0 --ignore-installed  --force-reinstall

ansible --version
ansible-playbook --version
